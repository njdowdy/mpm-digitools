#!/bin/bash

# Function to rename files with BOM characters in their filenames
rename_files_with_bom() {
  local dir="$1"
  
  # Loop through all files and directories within the given directory
  for file in "$dir"/*; do
    # Extract just the filename from the full path
    filename=$(basename -- "$file")
    
    # Check if the filename starts with the BOM characters
    if [[ $filename == $'\357\273\277'* ]]; then
      # Remove the BOM characters to form the new filename
      new_filename=${filename#$'\357\273\277'}
      
      # Rename the file
      mv -v "$file" "$dir/$new_filename"
    fi
  done
}

# Function to recursively traverse directories and subdirectories
traverse_dirs() {
  local dir="$1"
  
  rename_files_with_bom "$dir"
  
  # Loop through all the subdirectories
  for subdir in "$dir"/*; do
    if [ -d "$subdir" ]; then
      traverse_dirs "$subdir"
    fi
  done
}

# Entry point - start the script from the current directory
traverse_dirs "."

